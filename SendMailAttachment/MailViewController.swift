//
//  ViewController.swift
//  SendMailAttachment
//
//  Created by Denis on 07.02.2020.
//  Copyright © 2020 Denis. All rights reserved.
//

import UIKit
import MessageUI

class MailViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendEmail(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let  mailComposer = MFMailComposeViewController()
            mailComposer.setSubject("Title")
            
            let  htmlString = "<title>Пример веб-страницы</title></head><body><h1>Заголовок</h1><!-- Комментарий --><p>Первый абзац.Пример html страницы</p><html><body><p>Этот параграф содержит множество строк в исходном коде,но браузер игнорирует это.</p></body><p>Второй абзац.</p></body></html>"
            
            mailComposer.setToRecipients(["denvitios@gmail.com"])
            mailComposer.setMessageBody( htmlString, isHTML: true)
            mailComposer.mailComposeDelegate = self
            self.present(mailComposer, animated: true, completion: nil)
        } else {
            print("Email is not configured in settings app or we are not able to send an email")
        }
    }
    
    //MARK: - MailComposeDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            print("User cancelled")
            break
        case .saved:
            print("Mail is saved by user")
            break
        case .sent:
            print("Mail is sent successfully")
            break
        case .failed:
            print("Sending mail is failed")
            break
        default:
            break
        }
        controller.dismiss(animated: true)
    }
}

